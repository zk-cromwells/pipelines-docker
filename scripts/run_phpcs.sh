#!/bin/bash

git fetch origin master:refs/remotes/origin/master

files=$(git --no-pager diff --name-only --diff-filter=ACMRTUXB refs/remotes/origin/master)

if [ -z "$files" ]; then
	echo 'No files to check coding standards';
else
	files=$(git --no-pager diff --name-only --diff-filter=ACMRTUXB refs/remotes/origin/master | xargs -d '\n' -P4 printf "$(pwd)/%s\n")
	phpcs -p --standard=PSR12 --ignore=vendor/* $files
fi
