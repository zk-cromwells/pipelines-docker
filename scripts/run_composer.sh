#!/bin/bash

if test -f "composer.json"; then
    composer install --dry-run
fi
