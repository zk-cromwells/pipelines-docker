FROM php:7.2-cli

RUN apt-get update && apt-get install -y --no-install-recommends \
	wget ca-certificates curl zip unzip git \
    && apt-get clean \
    && rm -r /var/lib/apt/lists/*

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN curl -OL https://squizlabs.github.io/PHP_CodeSniffer/phpcs.phar
RUN mv phpcs.phar /usr/local/bin/phpcs && chmod +x /usr/local/bin/phpcs

ENV TZ Europe/London

COPY scripts/*.sh /usr/local/bin/
